module Main where

import XMonad.JH.Config (config)
import XMonad.JH.Main (xmonadjh)

main :: IO ()
main = xmonadjh config
