module XMonad.JH.Startup (startupHook) where

import XMonad.Core (X, spawn)
import XMonad.Util.SpawnOnce (spawnOnce)

startupHook :: X ()
startupHook = do
  spawnOnce "xsetroot -cursor_name left_ptr"
  spawnOnce "status-notifier-watcher"
  spawn "pkill xmobar; xmobar"
  spawnOnce "nitrogen --restore"
  spawnOnce "nm-applet --sm-disable --indicator"
  spawnOnce "blueman-applet"
  -- spawnOnce "discord --start-minimized"
  spawnOnce "steam -silent"
  spawnOnce "nextcloud --background"
  spawnOnce "easyeffects --gapplication-service"

  spawn "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1"
