{-# LANGUAGE FlexibleContexts #-}

module XMonad.JH.Main (xmonadjh) where

import XMonad (Window, XConfig)
import XMonad.Core (LayoutClass)
import XMonad.Hooks.EwmhDesktops (ewmh, ewmhFullscreen)
import XMonad.Hooks.ManageDocks (docks)
import XMonad.Hooks.TaffybarPagerHints (pagerHints)
import XMonad.Main (xmonad)
import XMonad.Hooks.DynamicLog (xmobarProp)

xmonadjh :: (LayoutClass l Window, Read (l Window)) => XConfig l -> IO ()
xmonadjh =
  xmonad
    . ewmhFullscreen
    . ewmh
    . docks
    . pagerHints
    . xmobarProp
