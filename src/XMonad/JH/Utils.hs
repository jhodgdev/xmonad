module XMonad.JH.Utils (homeDir, scriptsDir) where

homeDir :: String -> String
homeDir = (++) "/home/jack/"

scriptsDir :: String -> String
scriptsDir = (++) "/home/jack/.local/scripts/"
