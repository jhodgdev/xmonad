module XMonad.JH.Config (config) where

import XMonad (XConfig (..), mod4Mask)
import XMonad.Config (Default (def))
import XMonad.JH.Keybinds (keybinds)
import XMonad.JH.Layouts (LayoutHook)
import qualified XMonad.JH.Layouts as JH (layoutHook)
import qualified XMonad.JH.ManageHook as JH (manageHook)
import qualified XMonad.JH.Startup as JH (startupHook)
import XMonad.Util.EZConfig (additionalKeysP)

config :: XConfig LayoutHook
config = conf `additionalKeysP` keybinds
 where
  conf =
    def
      { -- Assign kitty as the default terminal.
        terminal = "kitty"
      , -- Assign Super to be the modifier key.
        modMask = mod4Mask
      , startupHook = JH.startupHook
      , layoutHook = JH.layoutHook
      , manageHook = JH.manageHook
      , borderWidth = 1
      , focusFollowsMouse = True
      }
