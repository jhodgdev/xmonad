module XMonad.JH.Colours (
  green,
  orange,
  red,
  lowWhite,
  yellow,
  white,
  blue,
  grey,
) where

green :: String
green = "#6dcfa7"

orange :: String
orange = "#f08437"

red :: String
red = "#fc4128"

lowWhite :: String
lowWhite = "#a6a6a6"

yellow :: String
yellow = "#fffa7d"

white :: String
white = "#fafbfc"

blue :: String
blue = "#5294e2"

grey :: String
grey = "#4a4c59"
