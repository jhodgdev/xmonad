module XMonad.JH.ManageHook (manageHook) where

import XMonad.Core (ManageHook)
import XMonad.Hooks.ManageHelpers
  ( composeOne,
    doFullFloat,
    doHideIgnore,
    isDialog,
    isFullscreen,
    transience,
    (-?>),
  )
import XMonad.ManageHook (doFloat, title, (=?))

manageHook :: ManageHook
manageHook =
  composeOne
    [ title =? "SteamTinkerLaunch-OpenSettings" -?> doFloat,
      title =? "Victoria 2" -?> doFloat,
      title =? "Wine System Tray" -?> doHideIgnore,
      isFullscreen -?> doFullFloat,
      transience,
      isDialog -?> doFloat
    ]
