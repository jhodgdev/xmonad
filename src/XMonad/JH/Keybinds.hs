{-# LANGUAGE ScopedTypeVariables #-}

module XMonad.JH.Keybinds (keybinds) where

import System.Exit (exitSuccess)
import XMonad.Actions.PhysicalScreens
  ( PhysicalScreen (P),
    horizontalScreenOrderer,
    sendToScreen,
    viewScreen,
  )
import XMonad.Core (X, io, spawn)
import XMonad.JH.Utils (homeDir, scriptsDir)
import XMonad.Operations (restart)

type Keybind = (String, X ())

keybinds :: [Keybind]
keybinds =
  concat
    [ launchers,
      applications,
      monitorCtrls,
      sessionCtrls,
      mediaCtrls
    ]

-- | Applications launchers e.g. rofi.
launchers :: [Keybind]
launchers =
  [ ("M-p", spawn "rofi -show drun"),
    ("M-S-p", spawn "rofi -show run")
  ]

-- | Frequently-used applications I want easy-access to.
applications :: [Keybind]
applications =
  [ ("M-d", spawn "emacsclient -c -a ''"), -- D is for DOOM!
    ("M-f", spawn "firefox"),
    ("M-g", spawn "nemo"),
    ("M-<Return>", spawn "kitty")
  ]

-- | Keybinds for interacting with the XMonad session.
sessionCtrls :: [Keybind]
sessionCtrls =
  [ ("M-<Backspace>", spawn $ scriptsDir "lock.sh"),
    ("M-q", restart "xmonad" True),
    ("M-S-q", io exitSuccess)
  ]

-- | Keybinds for interacting with media files e.g.
-- |    changing volume levels.
mediaCtrls :: [Keybind]
mediaCtrls =
  [ ("<XF86AudioMute>", spawn "pamixer --toggle-mute"),
    ("M-<F1>", spawn "pamixer --toggle-mute"),
    ("<XF86AudioLowerVolume>", spawn volumeDown),
    ("M-<F2>", spawn volumeDown),
    ("<XF86AudioRaiseVolume>", spawn volumeUp),
    ("M-<F3>", spawn volumeUp),
    ("<XF86AudioPlay>", spawn "playerctl play-pause"),
    ("M-<F4>", spawn "playerctl play-pause"),
    ("<XF86AudioPrev>", spawn "playerctl previous"),
    ("M-<F5>", spawn "playerctl previous"),
    ("<XF86AudioNext>", spawn "playerctl next"),
    ("M-<F6>", spawn "playerctl next")
  ]
  where
    volumeUp :: String = "wpctl set-volume @DEFAULT_SINK@ 4%+"
    volumeDown :: String = "wpctl set-volume @DEFAULT_SINK@ 4%-"

-- | Keybinds for interacting with my monitors.
monitorCtrls :: [Keybind]
monitorCtrls =
  let hso = horizontalScreenOrderer
   in [ ("M-w", viewScreen hso (P 0)),
        ("M-S-w", sendToScreen hso (P 0)),
        ("M-e", viewScreen hso (P 1)),
        ("M-S-e", sendToScreen hso (P 1)),
        ("M-r", viewScreen hso (P 2)),
        ("M-S-r", sendToScreen hso (P 2))
      ]
