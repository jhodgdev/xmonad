{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ScopedTypeVariables #-}

module XMonad.JH.Layouts (LayoutHook, layoutHook) where

import XMonad (Window)
import XMonad.Hooks.ManageDocks (AvoidStruts, avoidStruts)
import XMonad.Layout (Choose, Full (Full), Mirror (Mirror), Tall (Tall), (|||))
import XMonad.Layout.LayoutModifier (ModifiedLayout)
import XMonad.Layout.NoBorders (
  SmartBorder,
  WithBorder,
  noBorders,
  smartBorders,
 )
import XMonad.Layout.PerScreen (PerScreen, ifWider)
import XMonad.Layout.Renamed (Rename (CutWordsLeft, Replace), renamed)
import XMonad.Layout.SimplestFloat (SimplestFloat, simplestFloat)
import XMonad.Layout.Spacing (Spacing, spacingWithEdge)
import XMonad.Layout.WindowArranger (WindowArranger)

-- | Layout 'hook' passed to main config. Used to determine how
-- windows should be arranged.
layoutHook :: LayoutHook Window
layoutHook =
  smartBorders
    . renamed [CutWordsLeft 1] -- Delete 'Spacing' from layout name.
    . avoidStruts
    . spacingWithEdge 0
    $ ifWider 1200 landscapeL portraitL

type LayoutHook =
  ModifiedLayout
    SmartBorder
    ( ModifiedLayout
        Rename
        ( ModifiedLayout
            AvoidStruts
            (ModifiedLayout Spacing (PerScreen LandscapeL PortraitL))
        )
    )

-- | Layout options for landscape displays.
-- 1. Tall
-- 2. Full
-- 3. Float
landscapeL :: (Eq a) => LandscapeL a
landscapeL = tall ||| full ||| float

type LandscapeL = Choose Tall (Choose Full SimpFloat)

-- | Layout options for portrait displays.
-- 1. Wide
-- 2. Full
-- 3. Float
portraitL :: (Eq a) => PortraitL a
portraitL = wide ||| full ||| float

type PortraitL = Choose Wide (Choose Full SimpFloat)

-- | 'Tall' layout: default for landscape displays.
tall :: Tall a
tall =
  let
    --  Only have a single 'Master' pane.
    masterPanes :: Int = 1
    -- Alter the master/slave split by 3% every time it's changed.
    delta :: Rational = 3 / 100
    --  Have the master pane take up half the width of the display.
    paneRatio :: Rational = 1 / 2
   in
    Tall masterPanes delta paneRatio

-- | Mirrored variant of 'tall': default for portrait displays.
wide :: Wide a
wide = renamed [Replace "Wide"] $ Mirror tall

type Wide = ModifiedLayout Rename (Mirror Tall)

-- | Every window is presented 'full-screen', and stacked atop one another.
full :: Full a
full = Full

-- | Floating layout.
float :: (Eq a) => SimpFloat a
float = renamed [Replace "Float"] simplestFloat

type SimpFloat =
  ModifiedLayout
    Rename
    (ModifiedLayout WindowArranger SimplestFloat)
